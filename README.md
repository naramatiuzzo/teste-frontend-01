# Teste Seox

## Get Started

Para a aplicação é necessário possuir o Node 18(LTS) instalado na máquina, após isso basta instalar as dependencias rodando o seguinte comando:

```sh
    npm install
```

caso possua o yarn instalado bastar rodar o comando abaixo:

```sh
    yarn install
```

e para iniciar a aplicação basta rodar o comando a seguir:

```sh
    npm run dev
```

ou:

```sh
    yarn dev
```

## Observações

- O React possuí alguns problemas para trabalhar com SEO, porém foi feito uma implementação básica do mesmo.
- Vale ressaltar que nunca implementei nada de SEO anteriormente, sendo esta a primeira vez 😅😆. 
