
import Writers from './components/Writers'
import CardVideo from './components/CardVideo'
import Button from './components/Button'
import Slider from "react-slick";
import './style.scss'
import { useRef } from 'react';

const sliderBannerSettings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 1.2,
  slidesToScroll: 1,
  initialSlide: 0,
  arrows: false
}

const sliderSettings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 3,
  initialSlide: 0,
  arrows: false,
  variableWidth: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};

function App() {
  const sliderRef = useRef(null)

  function next(){
    sliderRef.current.slickNext()
  }

  function previous(){
    sliderRef.current.slickPrev()
  }

  return (
      <>        
       <div className="container-fluid mt-5">

        <section className="banner">
          
          <div className="header">
              <div className="row">
                <div className="col-6 col-sm-4 d-flex justify-content-left align-items-center">
                  <p className="title">Vídeos</p>
                </div>
                <div className="col-6 col-sm-8">
                  <div className="row">
                    <div className="col-9 col-lg-11 p-0 d-none d-md-flex d-flex justify-content-end align-items-center">
                      <Button text='Ver Mais'/>
                    </div>

                    <div className="col-3 col-lg-1 p-0 d-none d-md-flex d-flex justify-content-end align-items-center">
                        <Button className='margin-y' color='black' arrow='top' disabled/>
                        <Button className='margin-y' color='black' arrow='bottom'/>
                    </div>

                    <div className="col-12 d-md-none d-flex justify-content-end">
                      <Button color='black' arrow='right' />
                    </div>
                  </div>
                </div>
              </div>
            </div>

          <div className="item-videos">
            <div className="row d-none d-md-flex">
              <div className="col-md-8 col-lg-10">
                <CardVideo banner/>
              </div>

              <div className="col-md-4 col-lg-2">
                <div className="row mb-2">
                  <CardVideo/>
                </div>
                
                <div className="row mb-2">
                  <CardVideo/>
                </div>
              </div>
            </div>

            <div className="row d-block d-md-none">
              <div className="col-md-12">
                <CardVideo />
              </div>
              <div className="col-md-12 mt-5">
                <Slider {...sliderBannerSettings}>
                  <CardVideo />
                  <CardVideo />
                  <CardVideo />
                  <CardVideo />
                  <CardVideo />
                </Slider>
              </div>           

            </div>               

                
                
          </div>
        </section>

        <section className="writers">
              <div className="writers-header">
                <div className="writers-title">
                  <div className="row">
                    <div className="col-6 d-flex justify-content-left align-items-center">
                      <p className="title">Colunistas</p>
                    </div>

                    <div className="col-6">
                      <div className="p-0 d-flex justify-content-end align-items-center">
                          <Button className='margin-y d-none d-md-flex' color='gray' arrow='left'  onClick={previous}/>
                          <Button className='margin-y' color='gray' arrow='right' onClick={next}/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <div className="writers-container mt-3">
              <div className="writers-items">
                <Slider ref={sliderRef} {...sliderSettings}>
                  <div>
                    <Writers/>
                  </div>
                  <div>
                    <Writers/>
                  </div>
                  <div>
                    <Writers/>
                  </div>
                  <div>
                    <Writers/>
                  </div>
                  <div>
                    <Writers/>
                  </div>
                  <div>
                    <Writers/>
                  </div>
                </Slider>
              </div>
          </div>
        </section>
       </div>
       
      </>
  )
}

export default App
