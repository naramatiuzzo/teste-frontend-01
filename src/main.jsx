import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import './styles/index.scss'
import { Helmet } from 'react-helmet'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Helmet>
      <title>Teste Seox</title>
      <meta name='description' content='Teste frontend para a empresa Seox'/>
      <meta name='keywords' content='teste, seox, frontend, noticias'/>
    </Helmet>
    <App />
  </React.StrictMode>
)
