import CommentIcon from '../../assets/img/icon_comments.svg'
import Play from '../../assets/img/play.svg'
import { ReactSVG } from 'react-svg'
import './style.scss'

function CardVideo({src, banner= false}) {
    return (
        <div className="container-fluid">
    
            
            {
                !banner ? (
                    <div className="card-video">
                        <div className="card-img">
                            <div className="row">
                                <div className="col-md-12">
                                    <ReactSVG className="img-play" src={Play} />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12 comments-box">
                                <div className="comments">
                                        <img src={CommentIcon} alt="Icone de comentários" />
                                        <p className='m-0'>29</p>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div className="card-about">
                            <p className='title'>Lorem ipsum</p>
                            <p className='text'>Lorem ipsum dolor sit amet consectetur adipiscing elit ut nunc</p>
                        </div>
                    </div>
                ) : (
                    <div className="banner-card-video">
                        <div className="info-banner-card-video">
                            <div className="row">
                                <div className="col-md-12">
                                    <ReactSVG className="img-play" src={Play} />
                                </div>
                            </div>
    
                            <div className="row">
                                <p className='title-pricipal-banner'>Lorem ipsum</p>
                            </div>
                            
                            <div className="row">
                                <p className='text-principal-banner'>Lorem ipsum dolor sit amet consectetur adipiscing elit ut nunc nulla accumsan id</p>
                            </div>
                        </div>
                     </div>
                )
            }


        


        </div>
    )
}
export default CardVideo