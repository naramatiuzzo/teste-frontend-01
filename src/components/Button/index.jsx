import Arrow from '../../assets/img/arrow.svg'
import { ReactSVG } from 'react-svg'
import './style.scss'


function Button ({className, onClick, text, arrow = 'left' | 'right' | 'top' | 'bottom', color = 'gray' | 'black', disabled = false}) {
    return (
        <>
            {
                text ? (
                    <button className="button">
                        <p>{text}</p>
                        <ReactSVG src={Arrow} className="arrow"/>
                    </button>
                ) : (
                    <div className={`buttons-${color} ${className}`} >
                        <button className={`button-${color}`} disabled={disabled} onClick={onClick}>
                            <ReactSVG src={Arrow} className={`arrow arrow-${arrow}`}/>
                        </button>
                    </div>
                )
            }
        </>
    )
}
export default Button