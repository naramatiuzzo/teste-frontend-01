import { useState } from 'react'
import Picture from '../../assets/img/picture.png'
import './style.scss'

function Writers({className}) {
    const [open, setOpen] = useState(false);

    return(
        <div className={className}>
            <div className="card" onMouseEnter={() => setOpen(true)} onMouseLeave={() => setOpen(false)}>
                <div className="row">
                    <div className={`col-md-${open ? '3' : '12'}`}>
                        <div className="card-profile">
                                <div className="picture">
                                    <img src={Picture} alt="Foto do colunista Felipe Bittencourt" />
                                </div>
                                <div className="name">Felipe <br/> Bittencourt</div>
                        </div>
                    </div>
                    
                    <div className="col-md-9" >
                        <div className="card-content">
                            <p className="subtitle">DATA CAP É O NOVO MARKET CAP</p>
                            <p className='title'>A certificação de dados desestimula vazamentos de informações</p>
                            <p className="text">
                                Deep tech brasileira criou um método para reciclar ondas eletromagnéticas dispersas no ar como fonte de energia para dispositivos IoT. 
                            </p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    )
}
export default Writers